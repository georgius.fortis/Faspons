ACTOR BFG : WeaponBase replaces BFG9000 {
	Weapon.SelectionOrder 2800
	Weapon.AmmoType1 "Cell"
	Weapon.AmmoGive1 40
	Weapon.AmmoUse1 1
	Weapon.AmmoType2 "BFGCell"
	Weapon.AmmoUse2 1
	Weapon.UpSound "weapons/bfg/pickup"
	+WEAPON.AMMO_OPTIONAL
	Obituary "%o was turned inside out by %k's BFG"
	Inventory.PickupMessage "$GOTBFG9000"
	Inventory.PickupSound "weapons/bfg/pickup"
	Tag "$TAG_BFG9000"
	States {
		Ready:
			DBFG A 0 A_TakeInventory("SlidePlayer", 1)
			DBFG A 0 A_TakeInventory("BFGStage1", 1)
			DBFG A 0 A_TakeInventory("BFGStage2", 1)
			DBFG A 0 A_TakeInventory("BFGStage3", 1)
			DBFG A 0 A_TakeInventory("BFGStage4", 1)
			DBFG A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			DBFG A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			DBFG A 1 A_WeaponReady(WRF_ALLOWRELOAD)
			Loop
		Deselect:
			DBFG A 1 A_WeaponOffset(2, 34)
			DBFG A 1 A_WeaponOffset(7, 39)
			DBFG A 1 A_WeaponOffset(10, 47)
			DBFG A 1 A_WeaponOffset(22, 58)
			DBFG A 1 A_WeaponOffset(32, 69)
			DBFG A 1 A_WeaponOffset(54, 81)
			DBFG A 1 A_WeaponOffset(67, 120)
			DBFG A 1 A_Lower
			Wait
		Select:
			BFGR L 1 A_Raise
			BFGR L 1 A_WeaponOffset(67, 100)
			BFGR L 1 A_WeaponOffset(54, 81)
			BFGR M 1 A_WeaponOffset(32, 69)
			BFGR N 1 A_WeaponOffset(22, 58)
			BFGR O 1 A_WeaponOffset(10, 47)
			BFGR P 1 A_WeaponOffset(7, 39)
			BFGR Q 1 A_WeaponOffset(2, 34)
			BFGR A 0 {
        A_PlaySound("weapons/plasmagun/beep",7);
        A_GunFlash;
      }
			Goto Ready
		Fire:
			DBFC A 0 A_JumpIfInventory("BFGCell",1,1)
			Goto Reload
			DBFC A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			DBFC A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			DBFC A 0 A_PlaySound("weapons/bfg/classic", CHAN_WEAPON)
			DBFC A 0 A_PlaySound("weapons/bfg/start", 2)
			DBFC A 0 A_GiveInventory("BFGStage1", 1)
			DBFC A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_Light1
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("Load1")
			Goto HyperPower
		Load1:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("Load2")
			Goto HyperPower
		Load2:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("Load3")
			Goto HyperPower
		Load3:
			DBFC A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("Load4")
			Goto HyperPower
		Load4:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("Load5")
			Goto HyperPower
		Load5:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("Load6")
			Goto HyperPower
		Load6:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("Load7")
			Goto HyperPower
		Load7:
			DBFC B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldPre")
			Goto HyperPower
		HoldPre:
			TNT1 A 0 A_JumpIfInventory("BFGCell",26,1)
			Goto HyperPower
			TNT1 A 0 A_TakeInventory("BFGStage1", 1)
			TNT1 A 0 A_GiveInventory("BFGStage2", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldLoad1")
			Goto HyperPower
		HoldLoad1:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldLoad2")
			Goto HyperPower
		HoldLoad2:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldLoad3")
			Goto HyperPower
		HoldLoad3:
			DBFC C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldLoad4")
			Goto HyperPower
		HoldLoad4:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldLoad5")
			Goto HyperPower
		HoldLoad5:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldLoad6")
			Goto HyperPower
		HoldLoad6:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("HoldLoad7")
			Goto HyperPower
		HoldLoad7:
			DBFC D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3")
			Goto HyperPower
		FireStage3:
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,1)
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("BFGCell",31,1)
			Goto HyperPower
			TNT1 A 0 A_TakeInventory("BFGStage2", 1)
			TNT1 A 0 A_GiveInventory("BFGStage3", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_Light2
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load1")
			Goto HyperPower
		FireStage3Load1:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load2")
			Goto HyperPower
		FireStage3Load2:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load3")
			Goto HyperPower
		FireStage3Load3:
			DBFC E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load4")
			Goto HyperPower
		FireStage3Load4:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load5")
			Goto HyperPower
		FireStage3Load5:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load6")
			Goto HyperPower
		FireStage3Load6:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage3Load7")
			Goto HyperPower
		FireStage3Load7:
			DBFC F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("BFGMuzzSpawn",0,0,0,0)
			TNT1 A 0 A_ReFire("FireStage4")
			Goto HyperPower
		FireStage4:
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,1)
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("BFGCell",36,1)
			Goto HyperPower
			TNT1 A 0 A_TakeInventory("BFGStage3", 1)
			TNT1 A 0 A_GiveInventory("BFGStage4", 1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFC GHIJ 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto HyperPower
		HyperPower:
			TNT1 A 0 A_JumpIfInventory("BFGStage1",0,"Power1")
			TNT1 A 0 A_JumpIfInventory("BFGStage2",0,"Power2")
			TNT1 A 0 A_JumpIfInventory("BFGStage3",0,"Power3")
			TNT1 A 0 A_JumpIfInventory("BFGStage4",0,"Power4")
			Goto Ready
		Power1:
			TNT1 A 0 A_TakeInventory("BFGStage1", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.985)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("BFGCell",25,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 7)
			TNT1 AAAAAA 0 A_FireProjectile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 A_WeaponOffset(0, 34)
			TNT1 A 0 A_SetBlend("00 FF 00", 0.10, 10)
			DBFF B 1 BRIGHT A_FireProjectile("HyperNova",0,0,0,0)
			TNT1 A 0 A_Recoil(2)
			TNT1 A 0 {
        A_WeaponOffset(0, 33);
        A_ZoomFactor(0.99);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 {
        A_WeaponOffset(0, 32);
        A_ZoomFactor(0.995);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 {
        A_WeaponOffset(0, 32);
        A_ZoomFactor(1.00);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 A_WeaponOffset(0, 32)
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,2)
			TNT1 A 0 A_GiveInventory("BFGEmpty", 1)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power2:
			TNT1 A 0 A_TakeInventory("BFGStage2", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.9875)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("BFGCell",30,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 7)
			TNT1 AAAAAA 0 A_FireProjectile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 A_WeaponOffset(0, 36)
			TNT1 A 0 A_SetBlend("00 FF 00", 0.20, 20)
			DBFF B 1 BRIGHT A_FireProjectile("HyperNova2",0,0,0,0)
			TNT1 A 0 A_Recoil(3)
			TNT1 A 0 {
        A_WeaponOffset(0, 35);
        A_ZoomFactor(0.9875);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 {
        A_WeaponOffset(0, 34);
        A_ZoomFactor(0.995);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 {
        A_WeaponOffset(0, 33);
        A_ZoomFactor(1.00);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 A_WeaponOffset(0, 32)
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,2)
			TNT1 A 0 A_GiveInventory("BFGEmpty", 1)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power3:
			TNT1 A 0 A_TakeInventory("BFGStage3", 1)
			TNT1 A 0 A_StopSound(1)
			TNT1 A 0 A_StopSound(2)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.985)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.975)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("BFGCell",35,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 7)
			TNT1 AAAAAA 0 A_FireProjectile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 A_WeaponOffset(0, 38)
			TNT1 A 0 A_SetBlend("00 FF 00", 0.30, 30)
			DBFF B 1 BRIGHT A_FireProjectile("HyperNova3",0,0,0,0)
			TNT1 A 0 A_Recoil(4)
			TNT1 A 0 {
        A_WeaponOffset(0, 36);
        A_ZoomFactor(0.985);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 {
        A_WeaponOffset(0, 34);
        A_ZoomFactor(0.995);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 {
        A_WeaponOffset(0, 33);
        A_ZoomFactor(1.00);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 A_WeaponOffset(0, 32)
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,2)
			TNT1 A 0 A_GiveInventory("BFGEmpty", 1)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Power4:
			TNT1 A 0 A_TakeInventory("BFGStage4", 1)
			TNT1 A 0 A_PlaySound("weapons/bfg/fire", CHAN_AUTO)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.965)
			DBFF A 1 BRIGHT A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_TakeInventory("BFGCell",40,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 7)
			TNT1 AAAAAA 0 A_FireProjectile("GunSmokeSpawner",0,0,0,-5)
			TNT1 A 0 A_WeaponOffset(0, 40)
			TNT1 A 0 A_SetBlend("00 FF 00", 0.40, 40)
			DBFF B 1 BRIGHT A_FireProjectile("HyperNova4",0,0,0,0)
			TNT1 A 0 A_Recoil(5)
			TNT1 A 0 {
        A_WeaponOffset(0, 38);
        A_ZoomFactor(0.98);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 {
        A_WeaponOffset(0, 36);
        A_ZoomFactor(0.995);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 {
        A_WeaponOffset(0, 34);
        A_ZoomFactor(1.00);
      }
			DBFF B 1 BRIGHT
			TNT1 A 0 A_WeaponOffset(0, 32)
			TNT1 A 0 A_Light0
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_PlaySound("weapons/plasmagun/beep",7)
			DBFG ABABBB 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIfInventory("BFGCell",1,2)
			TNT1 A 0 A_GiveInventory("BFGEmpty", 1)
			TNT1 A 0 A_ClearReFire
			Goto Ready
		Reload:
			DBFG A 0 A_JumpIfInventory("BFGCell", 0, "Ready")
			DBFG A 0 A_JumpIfNoAmmo("NoAmmo")
			DBFG A 0 A_JumpIfInventory("ReloadingEnabled", 1, "ReloadNow")
			Goto ReloadLoop
		ReloadNow:
			TNT1 A 0 A_PlaySound ("weapons/bfg/reload", CHAN_AUTO)
			TNT1 A 0 A_JumpIfInventory("BFGEmpty", 1, "MagDrop")
			Goto RelNext
			MagDrop:
				TNT1 A 0 A_FireProjectile("PulseCellDrop",-5,0,4,-10)
			RelNext:
				TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
				TNT1 A 0 A_FireProjectile("GunSmokeSpawner",0,0,0,-5)
				BFGR A 20 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
				BFGR BCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
				Goto ReloadLoop
		ReloadLoop:
			TNT1 A 0 A_GiveInventory("BFGCell")
			TNT1 A 0 A_TakeInventory("Cell",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIfInventory("BFGCell",0,"ReloadEnd")
			TNT1 A 0 A_JumpIfInventory("Cell",1,"ReloadLoop")
			Goto ReloadEnd
		ReloadEnd:
			TNT1 A 0 A_JumpIfInventory("ReloadingEnabled", 1, "ReloadEndNow")
			Goto Ready
		ReloadEndNow:
			BFGR E 1
			BFGR GH 3
			BFGR I 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			BFGR JK 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			BFGR LMNOPQ 5 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBFG A 5 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ClearReFire
			TNT1 A 0 A_TakeInventory("BFGEmpty", 1)
			Goto Ready
		ThrowGrenade:
			TNT1 A 0 A_TakeInventory("GrenadeToss",1)
			TNT1 A 0 A_JumpIfInventory("HandGrenadeAmmo",1,1)
			Goto Ready
			BFGR Q 1 A_WeaponOffset(-2, 34)
			BFGR O 1 A_WeaponOffset(-10, 47)
			BFGR M 1 A_WeaponOffset(-32, 69)
			BFGR L 1 A_WeaponOffset(-67, 100)
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS ABCCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSD D 1 A_PlaySound("GPIN")
			TOSS EF 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 8 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TNT1 A 0 A_Takeinventory("HandGrenadeAmmo",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS H 1 A_FireProjectile("ThrownGrenade",0,false,7,3,true,-6)
			TNT1 A 0 A_SetPitch(+2 + pitch)
			TOSS I 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS K 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS L 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 9 A_WeaponOffset(-67, 100)
			BFGR L 1 {
        A_WeaponOffset(-67, 100);
        A_PlaySound("weapons/bfg/pickup", 0);
      }
			BFGR M 1 A_WeaponOffset(-32, 69)
			BFGR O 1 A_WeaponOffset(-10, 47)
			BFGR Q 1 A_WeaponOffset(-2, 34)
			TNT1 A 0 A_TakeInventory("GrenadeToss",1)
			Goto Ready
		NoAmmo:
			"####" A 0 A_PlaySound("weapons/plasmagun/beep",0)
			"####" A 0 A_ReFire("NoAmmoFireLoop")
			NoAmmoReloadLoop:
				"####" A 1 A_WeaponReady(WRF_NOFIRE)
				"####" A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_RELOAD, "NoAmmoReloadLoop")
				Goto Ready
			NoAmmoFireLoop:
				"####" A 1 A_WeaponReady(WRF_NOFIRE)
				"####" A 0 A_ReFire("NoAmmoFireLoop")
				Goto Ready
		Spawn:
			DEBF A -1
			Stop
	}
}

ACTOR HyperNova : BFGBall
{
	Decal "BFGLightning"
	Radius 14
	Height 9
	Speed 90
	Alpha 0.95
	Scale 0.1
	Damage 40
	DamageType Plasma
	Translation "192:207=112:127"
	DeathSound "none"
	States
	{
		Spawn:
			BBAL ABCD 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_SpawnProjectile ("Residue", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0 A_BFGSpray ("BFGExtra", 25, 5)
			BBB1 ABCDE 4 Bright
			TNT1 A 0 A_PlaySound ("weapons/bfgx")
			RR00 ABCDEF 3 Bright
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_SpawnProjectile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0
			Stop
	}
}

ACTOR HyperNova2 : BFGBall
{
	Decal "BFGLightning"
	Radius 14
	Height 9
	Speed 85
	Alpha 0.95
	Scale 0.2
	Damage 60
	DamageType Plasma
	Translation "192:207=112:127"
	DeathSound "none"
	States
	{
		Spawn:
			BBAL ABCD 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_SpawnProjectile ("Residue", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0 A_BFGSpray ("BFGExtra", 30, 10)
			BBB1 ABCDE 4 Bright
			TNT1 A 0 A_PlaySound ("weapons/bfgx")
			RR00 ABCDEF 3 Bright
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_SpawnProjectile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0
			Stop
	}
}

ACTOR HyperNova3 : BFGBall
{
	Decal "BFGLightning"
	Radius 14
	Height 9
	Speed 80
	Alpha 0.95
	Scale 0.3
	Damage 80
	DamageType Plasma
	Translation "192:207=112:127"
	DeathSound "none"
	States
	{
		Spawn:
			BBAL ABCD 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_SpawnProjectile ("Residue", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0 A_BFGSpray ("BFGExtra", 35, 15)
			BBB1 ABCDE 4 Bright
			TNT1 A 0 A_PlaySound ("weapons/bfgx")
			RR00 ABCDEF 3 Bright
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_SpawnProjectile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0
			Stop
	}
}

ACTOR HyperNova4 : BFGBall
{
	Decal "BFGLightning"
	Radius 14
	Height 9
	Speed 75
	Alpha 0.95
	Scale 0.6
	Damage 100
	DamageType Plasma
	Translation "192:207=112:127"
	DeathSound "none"
	States
	{
		Spawn:
			BBAL ABCD 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_SpawnProjectile ("Residue", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0 A_BFGSpray ("BFGExtra", 40, 20)
			BBB1 ABCDE 4 Bright
			TNT1 A 0 A_PlaySound ("weapons/bfgx")
			RR00 ABCDEF 3 Bright
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_SpawnProjectile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			TNT1 A 0
			Stop
	}
}

ACTOR Nova : BFGBall
{
	Decal "Scorch"
	Radius 7
	Height 9
	Speed 50
	Alpha 0.95
	Scale 0.8
	Damage 20
	DamageType Plasma
	Translation "192:207=112:127"
	SeeSound	"none"
	DeathSound "weapons/plasmad/explode"
	States
	{
		Spawn:
			BBB1 E 4 BRIGHT
			Loop
		Death:
			TNT1 A 0 A_BFGSpray ("BFGExtra", 10, 2)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_SpawnProjectile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			VFX3 ABCD 2 Bright
			VFX3 EFGH 3 Bright
			VFX3 IJ 3
			Stop
	}
}

ACTOR Residue : BFGExtra
{
	+NOBLOCKMAP
	+NOGRAVITY
	RenderStyle Add
	Alpha 0.25
	Scale 0.4
	Damage 0
	DamageType "BFGSplash"
	States
	{
		Spawn:
			TNT1 A 0 A_BFGSpray ("BFGExtra", 5, 5)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_SpawnProjectile ("RocketSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			R044 JKLMNOPQJKLMNOPQJKLMNOPQJKLMNOPQ 5 A_FadeOut(0.02)
			Stop
	}
}

ACTOR BFGCell : Ammo
{
	+IGNORESKILL
	Inventory.MaxAmount 160
}

ACTOR BFGEmpty : Inventory {
	Inventory.MaxAmount 1
}

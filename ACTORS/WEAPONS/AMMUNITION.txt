ACTOR AmmoSpawner : RandomSpawner replaces Clip {
	DropItem "RifleAmmo", 255, 3
	DropItem "SmallBelt", 255, 1
}

ACTOR Ammobox : RandomSpawner replaces Clipbox {
	DropItem "NatoRifleBox", 255, 3
	DropItem "Nato", 255, 1
}

ACTOR RifleAmmo : Ammo {
	Inventory.Amount 15
	Inventory.MaxAmount 150
	Ammo.BackpackAmount 45
	Ammo.BackpackMaxAmount 300
	Inventory.PickupMessage "Collected a rifle magazine."
	Inventory.PickupSound "weapons/rifle/slap"
	States
	{
		Spawn:
			CLIP A -1
			Stop
	}
}

ACTOR Nato : Ammo {
	Inventory.Icon "BELSA0"
	Inventory.PickupSound "ammoboxup"
	Inventory.PickupMessage "Picked up a box of NATO."
	Inventory.Amount 50
	Inventory.MaxAmount 250
	Ammo.BackpackAmount 25
	Ammo.BackpackMaxAmount 500
	Scale 0.5
	States
	{
		Spawn:
			BELT A -1
			Stop
	}
}

ACTOR SmallBelt : CustomInventory {
	Inventory.PickupMessage "Picked up a NATO belt."
	Inventory.PickupSound "ammoboxup"
	Scale 0.7
	States {
		Spawn:
			SBEL A -1
			Stop
		Pickup:
			TNT1 A 0 A_GiveInventory ("Nato", 25)
			Stop
	}
}

ACTOR NatoRifleBox : CustomInventory {
	Inventory.PickupMessage "Picked up a box of NATO and some mags."
	Inventory.PickupSound "ammoboxup"
	Scale 0.5
	States
	{
		Spawn:
			NARF A -1
			Stop
		Pickup:
			TNT1 A 0 {
				if(CountInv("Bakpak") || CountInv("BakpakHeretic")) {
					if(CountInv("Nato") == 500 && CountInv("RifleAmmo") == 300) {
						return ResolveState("PickupStop");
					} else {
						if(CountInv("Nato") < 500) {
							A_GiveInventory ("Nato", 50);
						} else {
							A_SpawnItemEx("Nato", 0, 0, 0, 0, 0, 0, 0, SXF_SETTARGET);
						}

						if(CountInv("RifleAmmo") <= 270) {
							A_GiveInventory ("RifleAmmo", 30);
						} else if(CountInv("RifleAmmo") <= 285) {
							A_SpawnItemEx("RifleAmmo", 0, 0, 0, 0, 0, 0, 0, SXF_SETTARGET);
						} else {
							A_SpawnItemEx("RifleAmmo", 0, 0, 0, 0, 0, 0, 0, SXF_SETTARGET);
							A_SpawnItemEx("RifleAmmo", 0, 5, 0, 0, 0, 0, 0, SXF_SETTARGET);
						}

						return ResolveState("PickupFinish");
					}

				} else {
					if(CountInv("Nato") == 250 && CountInv("RifleAmmo") == 150) {
						return ResolveState("PickupStop");
					} else {
						if(CountInv("Nato") < 250) {
							A_GiveInventory ("Nato", 50);
						} else {
							A_SpawnItemEx("Nato", 0, 0, 0, 0, 0, 0, 0, SXF_SETTARGET);
						}

						if(CountInv("RifleAmmo") <= 120) {
							A_GiveInventory ("RifleAmmo", 30);
						} else if(CountInv("RifleAmmo") <= 135) {
							A_SpawnItemEx("RifleAmmo", 0, 0, 0, 0, 0, 0, 0, SXF_SETTARGET);
						} else {
							A_SpawnItemEx("RifleAmmo", 0, 0, 0, 0, 0, 0, 0, SXF_SETTARGET);
							A_SpawnItemEx("RifleAmmo", 0, 5, 0, 0, 0, 0, 0, SXF_SETTARGET);
						}

						return ResolveState("PickupFinish");
					}
				}
			}
		PickupStop:
			TNT1 A 0
			Fail
		PickupFinish:
			TNT1 A 0 {
				return true;
			}
			Stop
	}
}

ACTOR RocketClip : CustomInventory {
	Inventory.PickupMessage "Picked up a rocket clip."
	Inventory.PickupSound "weapons/rifle/slap"
	States
	{
		Spawn:
			RCSE A -1
			Stop
		Pickup:
			TNT1 A 0 A_GiveInventory ("RocketAmmo", 5)
	}
}

ACTOR Shells : Shell replaces Shell {
	Inventory.PickupSound "SHELLPU"
}

ACTOR BoxofShells : Shellbox replaces Shellbox {
	Inventory.PickupSound "SHELLPU"
}

ACTOR Cells : Cell Replaces Cell {
	Inventory.PickupSound "CPAKUP"
}

ACTOR CellPacks : CellPack Replaces CellPack {
	Inventory.PickupSound "CPAKUP"
}

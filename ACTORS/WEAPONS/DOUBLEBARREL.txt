ACTOR DoubleBarrel : WeaponBase replaces SuperShotgun {
	Weapon.SelectionOrder 700
	Weapon.AmmoType1 "Shell"
	Weapon.AmmoGive1 8
	Weapon.AmmoUse1 1
	Weapon.AmmoType2 "LoadedShells"
	Weapon.AmmoUse2 1
	DamageType "Buckshot"
	Weapon.UpSound "weapons/riotgun/raise"
	Inventory.PickupMessage "You got the Double Barrel Shotgun!"
	Inventory.PickupSound "weapons/shellslinger/close"
	Obituary "%o was buckshot by %k's double barrel"
	+WEAPON.AMMO_OPTIONAL
	Tag "Double Barrel Shotgun"
	Scale 1.2
	States {
		CloseDB:
			TNT1 A 0 A_JumpIfInventory("LoadedShells", 2, 1)
			Goto CloseDBSingle
			TNT1 A 0 A_PlayWeaponSound("weapons/shellslinger/close")
			DBCS B 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL CBA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 {
        A_TakeInventory("DBRaise", 1);
        A_GunFlash;
      }
			Goto Ready
		CloseDBSingle:
			TNT1 A 0 A_JumpIfInventory("LoadedShells", 1, 1)
			Goto CloseDBEmpty
			TNT1 A 0 A_PlayWeaponSound("weapons/shellslinger/close")
			DBCS D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL CBA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 {
        A_TakeInventory("DBRaise", 1);
        A_GunFlash;
      }
			Goto Ready
		CloseDBEmpty:
			TNT1 A 0 A_PlayWeaponSound("weapons/shellslinger/close")
			DBRL H 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL F 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL E 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL CBA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 {
        A_TakeInventory("DBRaise", 1);
        A_GunFlash;
      }
			Goto Ready
		Ready:
			DBSG A 0 A_JumpIfInventory("DBRaise", 1, "CloseDB")
			DBSG A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			DBSG A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			DBSG A 1 A_WeaponReady(WRF_ALLOWRELOAD)
			Loop
		Deselect:
			DBSG A 1 A_WeaponOffset(2, 34)
			DBSG A 1 A_WeaponOffset(7, 39)
			DBRL A 1 A_WeaponOffset(10, 47)
			DBRL A 1 A_WeaponOffset(22, 58)
			DBRL A 1 A_WeaponOffset(32, 69)
			DBRL A 1 A_WeaponOffset(54, 81)
			DBRL A 1 A_WeaponOffset(67, 100)
			DBRL A 1 A_WeaponOffset(81, 121)
			DBRL A 1 A_Lower
			Wait
		Select:
			TNT1 A 0 A_JumpIfInventory("LoadedShells", 2, 1)
			Goto SelectSingle
			TNT1 A 0 A_GiveInventory("DBRaise", 1)
			DBCS A 1 A_WeaponOffset(95, 100)
			DBCS A 1 A_WeaponOffset(68, 69)
			DBCS A 1 A_WeaponOffset(10, 47)
			DBCS A 1 A_WeaponOffset(2, 34)
			DBCS A 0 A_Raise
			Goto Ready
		SelectSingle:
			TNT1 A 0 A_JumpIfInventory("LoadedShells", 1, 1)
			Goto SelectEmpty
			TNT1 A 0 A_GiveInventory("DBRaise", 1)
			DBCS C 1 A_WeaponOffset(95, 100)
			DBCS C 1 A_WeaponOffset(68, 69)
			DBCS C 1 A_WeaponOffset(10, 47)
			DBCS C 1 A_WeaponOffset(2, 34)
			DBCS C 0 A_Raise
			Goto Ready
		SelectEmpty:
			TNT1 A 0 A_GiveInventory("DBRaise", 1)
			DBRL Z 1 A_WeaponOffset(95, 100)
			DBRL Z 1 A_WeaponOffset(68, 69)
			DBRL Z 1 A_WeaponOffset(10, 47)
			DBRL Z 1 A_WeaponOffset(2, 34)
			DBRL Z 0 A_Raise
			Goto Ready
		Fire:
			TNT1 A 0 A_JumpIfInventory("LoadedShells",0,1) //Fully loaded? Skip "FireSingle"
			Goto FireSingle
			TNT1 A 0 A_TakeInventory("LoadedShells",2,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_GunFlash("FlashFire")
			TNT1 A 0 A_PlaySound("weapons/kapow", CHAN_WEAPON)
			TNT1 A 0 A_FireBullets (11,7,20,5,"SShotgunPuff",0)
			TNT1 A 0 A_SetPitch (pitch-3.0)
			TNT1 A 0 A_ZoomFactor(0.95)
			DBSG A 1 A_WeaponOffset(0,58)
			TNT1 A 0 A_ZoomFactor(0.94)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableTracers", 0, 0, 0)==2, 21)
			TNT1 AAAAAAAAAAAAAAAAAAAA 0 A_FireProjectile("Tracer", random(-5,5), 0, 0, -13, 0, random(-3,3))
			DBSG A 1 A_WeaponOffset(0,61)
			TNT1 A 0 A_ZoomFactor(0.935)
			TNT1 A 0 A_AlertMonsters
			TNT1 A 0 A_FireProjectile("MuzzSpawn",0,0,0,0)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch (pitch-2.0)
			DBSG A 1 {
        A_WeaponOffset(0,62);
        A_SetPitch (pitch-1.0);
      }
			TNT1 A 0 A_ZoomFactor(0.955)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 3)
			TNT1 A 0 A_FireProjectile("SmokeSpawner",0,0,-2,0)
			TNT1 A 0 A_FireProjectile("SmokeSpawner",0,0,1,0)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 1 {
        A_WeaponOffset(0,54);
        A_SetPitch (pitch+2.0);
      }
			TNT1 A 0 A_ZoomFactor(0.965)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 5)
			TNT1 AAAA 0 A_FireProjectile("GunSmokeSpawner",0,0,0,5)
			DBSG A 1 {
        A_WeaponOffset(0,49);
        A_SetPitch (pitch+2.0);
      }
			TNT1 A 0 A_ZoomFactor(0.985)
			TNT1 A 0 A_AlertMonsters
			DBSG A 1 {
        A_WeaponOffset(0,45);
        A_SetPitch (pitch+2.0);
      }
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 1 A_WeaponOffset(0,36)
			DBSG A 1 A_WeaponOffset(0,32)
			TNT1 A 0 A_ZoomFactor(1.00)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto Reload
		FireSingle:
			DBSG A 0 A_JumpIfInventory("LoadedShells",1,1) //No loaded shells? Do not fire.
			Goto Reload
			TNT1 A 0 A_TakeInventory("LoadedShells",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_PlaySound("weapons/kapow", CHAN_WEAPON)
			TNT1 A 0 A_FireBullets (6,5,10,5,"SShotgunPuff",0)
			TNT1 A 0 A_SetPitch (pitch-3.0)
			TNT1 A 0 A_ZoomFactor(0.95)
			DBLF A 1 A_WeaponOffset(0,58)
			TNT1 A 0 A_ZoomFactor(0.94)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableTracers", 0, 0, 0)==2, 11)
			TNT1 AAAAAAAAAA 0 A_FireProjectile("Tracer", random(-3,3), 0, 0, -13, 0, random(-2,2))
			DBLF B 1 A_WeaponOffset(0,61)
			TNT1 A 0 A_ZoomFactor(0.935)
			TNT1 A 0 A_AlertMonsters
			TNT1 A 0 A_FireProjectile("MuzzSpawn",0,0,0,0)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch (pitch-2.0)
			DBSG A 1 {
        A_WeaponOffset(0,62);
        A_SetPitch (pitch-1.0);
      }
			TNT1 A 0 A_ZoomFactor(0.955)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 3)
			TNT1 A 0 A_FireProjectile("SmokeSpawner",0,0,-2,0)
			TNT1 A 0 A_FireProjectile("SmokeSpawner",0,0,1,0)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 1 {
        A_WeaponOffset(0,54);
        A_SetPitch (pitch+2.0);
      }
			TNT1 A 0 A_ZoomFactor(0.965)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 5)
			TNT1 AAAA 0 A_FireProjectile("GunSmokeSpawner",0,0,0,5)
			DBSG A 1 {
        A_WeaponOffset(0,49);
        A_SetPitch (pitch+2.0);
      }
			TNT1 A 0 A_ZoomFactor(0.985)
			TNT1 A 0 A_AlertMonsters
			DBSG A 1 {
        A_WeaponOffset(0,45);
        A_SetPitch (pitch+2.0);
      }
			TNT1 A 0 A_ZoomFactor(0.995)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 1 A_WeaponOffset(0,36)
			DBSG A 1 A_WeaponOffset(0,32)
			TNT1 A 0 A_ZoomFactor(1.00)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto Reload
		Reload: //Double shell reload
			DBSG A 0 A_JumpIfInventory("LoadedShells", 0, "Ready")
			DBSG A 0 A_JumpIfNoAmmo("NoAmmo") //Does not allow reload if you have 0 shells
		ReloadNow:
			TNT1 A 0 A_JumpIfInventory("LoadedShells", 1, "ReloadAdd") //Load an additional shell
			TNT1 A 0 A_JumpIfInventory("Shell", 2, 1) //Only load 1 shell if you have 1 shell in inv.
			Goto ReloadSingle
			DBRL A 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL E 3 A_PlaySound("weapons/shellslinger/open",5)
			DBRL F 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_FireProjectile("GunSmokeSpawner",0,0,-8,0)
			DBRL G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("SSGCasingSpawner",0,0,-10,-10)
			TNT1 A 0 A_FireProjectile("SSGCasingSpawner",0,0,-8,-10)
			DBRL Z 8 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto ReloadWorkD
		ReloadSingle: //Single shell reload
			DBRL A 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL E 3 A_PlaySound("weapons/shellslinger/open",5)
			DBRL F 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_FireProjectile("GunSmokeSpawner",0,0,-8,0)
			DBRL G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL H 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_FireProjectile("SSGCasingSpawner",0,0,-10,-10)
			DBRL Z 8 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto ReloadWorkS
		ReloadAdd: //Additional shell reload
			DBRL A 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL D 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL E 3 A_PlaySound("weapons/shellslinger/open",5)
			DBRL F 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_FireProjectile("GunSmokeSpawner",0,0,-8,0)
			DBRL G 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBCS D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBCS C 8 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto ReloadWorkA
		ReloadWorkD: //Double shell reload work
			TNT1 A 0 A_GiveInventory("LoadedShells")
			TNT1 A 0 A_TakeInventory("Shell",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIfInventory("LoadedShells",0,"ReloadEndD")
			TNT1 A 0 A_JumpIfInventory("Shell",1,"ReloadWorkD")
			Goto ReloadEndD
		ReloadWorkS: //Single shell reload work
			TNT1 A 0 A_GiveInventory("LoadedShells")
			TNT1 A 0 A_TakeInventory("Shell",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIfInventory("LoadedShells",0,"ReloadEndS")
			TNT1 A 0 A_JumpIfInventory("Shell",1,"ReloadWorkS")
			Goto ReloadEndS
		ReloadWorkA: //Additional shell reload work
			TNT1 A 0 A_GiveInventory("LoadedShells")
			TNT1 A 0 A_TakeInventory("Shell",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIfInventory("LoadedShells",0,"ReloadEndA")
			TNT1 A 0 A_JumpIfInventory("Shell",1,"ReloadWorkA")
			Goto ReloadEndA
		ReloadEndD: //Double shell reload end
			DBRL I 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL K 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL L 1 A_PlayWeaponSound("weapons/shellslinger/load")
			DBRL M 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 11 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL N 2 A_PlayWeaponSound("weapons/shellslinger/close")
			DBRL O 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL P 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL Q 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 3 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 1 A_ReFire("FireReload")
			Goto Ready
		ReloadEndS: //Single shell reload work
			DBRL I 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSR J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSR K 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSR L 1 A_PlayWeaponSound("weapons/shellslinger/load")
			DBRL M 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 11 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL N 2 A_PlayWeaponSound("weapons/shellslinger/close")
			DBRL O 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL P 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL Q 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 3 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 1 A_ReFire("FireReload")
			Goto Ready
		ReloadEndA: //Additional shell reload work
			DBAR I 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBAR J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBAR K 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL L 1 A_PlayWeaponSound("weapons/shellslinger/load")
			DBRL M 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 11 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL N 2 A_PlayWeaponSound("weapons/shellslinger/close")
			DBRL O 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL P 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBRL Q 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 3 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			DBSG A 1 A_ReFire("FireReload")
			Goto Ready
		ThrowGrenade:
			TNT1 A 0 A_TakeInventory("GrenadeToss",1)
			TNT1 A 0 A_JumpIfInventory("HandGrenadeAmmo",1,1)
			Goto Ready
			DBSG A 1 A_WeaponOffset(2, 34)
			//DBSG A 1 A_WeaponOffset(7, 39)
			DBRL A 1 A_WeaponOffset(10, 47)
			//DBRL A 1 A_WeaponOffset(22, 58)
			DBRL A 1 A_WeaponOffset(32, 69)
			//DBRL A 1 A_WeaponOffset(54, 81)
			DBRL A 1 A_WeaponOffset(67, 100)
			DBRL A 1 A_WeaponOffset(81, 121)
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS ABCCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSD D 1 A_PlaySound("GPIN")
			TOSS EF 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 8 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TNT1 A 0 A_Takeinventory("HandGrenadeAmmo",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS H 1 A_FireProjectile("ThrownGrenade",0,false,7,3,true,-6)
			TNT1 A 0 A_SetPitch(+2 + pitch)
			TOSS I 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS K 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS L 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 9 {
        A_WeaponOffset(95, 100);
        A_GiveInventory("DBRaise", 1);
      }
			DBCS A 1 A_WeaponOffset(95, 100)
			DBCS A 1 A_WeaponOffset(68, 69)
			DBCS A 1 A_WeaponOffset(10, 47)
			DBCS A 1 A_WeaponOffset(2, 34)
			TNT1 A 0 A_TakeInventory("GrenadeToss",1)
			Goto Ready
		NoAmmo:
			"####" A 0 A_PlaySound("bulletup",0)
			"####" A 0 A_ReFire("NoAmmoFireLoop")
			NoAmmoReloadLoop:
				"####" A 1 A_WeaponReady(WRF_NOFIRE)
				"####" A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_RELOAD, "NoAmmoReloadLoop")
				Goto Ready
			NoAmmoFireLoop:
				"####" A 1 A_WeaponReady(WRF_NOFIRE)
				"####" A 0 A_ReFire("NoAmmoFireLoop")
				Goto Ready
		FlashFire:
			SHT2 Y 2 Bright A_Light1
			SHT2 Z 1 Bright A_Light2
			TNT1 A 1 A_Light0
			Stop
		Spawn:
			DBBL A -1
		Stop
	}
}

ACTOR LoadedShells : Ammo
{
   +IGNORESKILL
   Inventory.MaxAmount 2
}

Actor DBRaise : Inventory {
	Inventory.MaxAmount 1
}

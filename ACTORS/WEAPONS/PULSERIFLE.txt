ACTOR PulseRifle : WeaponBase replaces PlasmaRifle {
	Weapon.SelectionOrder 100
	Weapon.AmmoType1 "Cell"
	Weapon.AmmoGive1 60
	Weapon.AmmoUse1 1
	Weapon.AmmoType2 "Cellpak"
	Weapon.AmmoUse2 1
	Weapon.UpSound "weapons/plasmagun/upsound"
	Inventory.PickupMessage "You got the Pulse Rifle!"
	Inventory.PickupSound "weapons/plasmagun/upsound"
	Obituary "%o was liquified by %k's Pulse Rifle"
	+WEAPON.AMMO_OPTIONAL
	Tag "Pulse Rifle"
	States {
		Ready:
			PLZG A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			PLZG A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			PLZG A 1 A_WeaponReady(WRF_ALLOWRELOAD)
			Loop
		Deselect:
			PLZG A 1 A_WeaponOffset(2, 34)
			PLZG A 1 A_WeaponOffset(7, 39)
			PLZR A 1 A_WeaponOffset(10, 47)
			PLZR B 1 A_WeaponOffset(22, 58)
			PLZR C 1 A_WeaponOffset(32, 69)
			PLZR D 1 A_WeaponOffset(54, 81)
			PLZR E 1 A_WeaponOffset(67, 120)
			PLZR E 0 A_Lower
			Wait
		Select:
			PLZR E 1 A_WeaponOffset(67, 100)
			PLZR D 1 A_WeaponOffset(54, 81)
			PLZR C 1 A_WeaponOffset(32, 69)
			PLZR B 1 A_WeaponOffset(22, 58)
			PLZR A 1 A_WeaponOffset(10, 47)
			PLZG A 1 A_WeaponOffset(7, 39)
			PLZG A 1 {
        A_WeaponOffset(2, 34);
        A_GunFlash;
      }
			PLZR E 0 A_Raise
			Goto Ready
		Fire:
			TNT1 A 0 A_JumpIfInventory("Cellpak",1,1)
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_TakeInventory("Cellpak",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_GunFlash("FlashFire")
			TNT1 A 0 A_AlertMonsters
			TNT1 A 0 A_PlaySound ("weapons/plasmagun/fire", CHAN_WEAPON)
			TNT1 A 0 A_SetPitch (pitch+0.1)
			TNT1 A 0 A_WeaponOffset(0, 36)
			TNT1 A 0 A_ZoomFactor(0.995)
			PLZA A 1 A_FireProjectile("Plasma", 0, 0, 0, 0, 0, 0)
			TNT1 A 0 A_ZoomFactor(0.99)
			TNT1 A 0 A_SetPitch (pitch-0.1)
			TNT1 A 0 A_WeaponOffset(0, 34)
			PLZF B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.985)
			TNT1 A 0 A_WeaponOffset(0, 32)
			PLZF B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.98)
			TNT1 A 0 A_JumpIfInventory("Cellpak",1,2)
			TNT1 A 0 A_GiveInventory("PulseEmpty", 1)
			PLZF CB 1 A_ReFire
			TNT1 A 0 A_ZoomFactor(0.985)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_FireProjectile("GunSmokeSpawner",0,0,-4,5)
			PLZG B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			PLZG C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			PLZG D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			PLZG EEEE 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZG DCB 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto Ready
		Hold:
			PLZG A 0 A_JumpIfInventory("Cellpak",1,3)
			PLZG A 0 A_JumpIfInventory("ReloadingEnabled", 1, "ReloadWind")
			PLZG A 0 A_JumpIfNoAmmo("NoAmmoWind")
			Goto Reload
			TNT1 A 0 A_JumpIfInventory("GrenadeToss",1,"ThrowGrenade")
			TNT1 A 0 A_JumpIfInventory("TossAmmo",1,"TossingAmmo")
			TNT1 A 0 A_TakeInventory("Cellpak",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_GunFlash("FlashFire")
			TNT1 A 0 A_AlertMonsters
			TNT1 A 0 A_PlaySound ("weapons/plasmagun/fire", CHAN_WEAPON)
			TNT1 A 0 A_SetPitch (pitch+0.1)
			TNT1 A 0 A_WeaponOffset(0, 36)
			PLZA A 1 A_FireProjectile("Plasma", 0, 0, 0, 0, 0, 0)
			TNT1 A 0 A_SetPitch (pitch-0.1)
			TNT1 A 0 A_WeaponOffset(0, 34)
			PLZF B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponOffset(0, 32)
			PLZF B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIfInventory("Cellpak",1,2)
			TNT1 A 0 A_GiveInventory("PulseEmpty", 1)
			PLZF CB 1 A_ReFire
			TNT1 A 0 A_ZoomFactor(0.985)
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_FireProjectile("GunSmokeSpawner",0,0,-4,5)
			PLZG B 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.99)
			PLZG C 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(0.995)
			PLZG D 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_ZoomFactor(1.00)
			PLZG EEEE 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZG DCB 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			Goto Ready
		FlashFire:
			PLZA A 1 Bright A_Light1
			TNT1 A 1 A_Light0
			Stop
		UnZoomFlash:
			TNT1 A 1 A_ZoomFactor(0.985)
			TNT1 A 1 A_ZoomFactor(0.99)
			TNT1 A 1 A_ZoomFactor(0.995)
			TNT1 A 1 A_ZoomFactor(1.00)
			Stop
		ReloadWind:
			TNT1 A 0 A_GunFlash("UnZoomFlash")
		Reload:
			PLZG A 0 A_JumpIfInventory("Cellpak", 0, "Ready")
			PLZG A 0 A_JumpIfNoAmmo("NoAmmo")
			PLZG A 0 A_JumpIfInventory("ReloadingEnabled", 1, "ReloadNow")
			Goto ReloadLoop
		ReloadNow:
			TNT1 A 0 A_PlaySound ("weapons/plasmagun/dryfire", CHAN_AUTO)
			TNT1 A 0 A_PlaySound ("weapons/plasmagun/reload")
			PLZR ABJII 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZR HG 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_JumpIfInventory("PulseEmpty", 1, "MagDrop")
			Goto RelNext
			MagDrop:
				TNT1 A 0 A_FireProjectile("PulseCellDrop",-5,0,-2,-10)
			RelNext:
				PLZR EEEEEEEEEEEEEF 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
				Goto ReloadLoop
		ReloadLoop:
			TNT1 A 0 A_GiveInventory("Cellpak")
			TNT1 A 0 A_TakeInventory("Cell",1,TIF_NOTAKEINFINITE)
			TNT1 A 0 A_JumpIfInventory("Cellpak",0,"ReloadEnd")
			TNT1 A 0 A_JumpIfInventory("Cell",1,"ReloadLoop")
			Goto ReloadEnd
		ReloadEnd:
			TNT1 A 0 A_JumpIfInventory("ReloadingEnabled", 1, "ReloadEndNow")
			Goto Ready
		ReloadEndNow:
			PLZR GHI 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZR J 3 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZR M 4 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZR NO 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZR BA 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZG J 6 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			PLZG A 2 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 {
				A_TakeInventory("PulseEmpty", 1);
				A_ClearRefire;
			}
			Goto Ready
		ThrowGrenade:
			TNT1 A 0 A_TakeInventory("GrenadeToss",1)
			TNT1 A 0 A_JumpIfInventory("HandGrenadeAmmo",1,1)
			Goto Ready
			PLZG A 1 A_WeaponOffset(2, 34)
			PLZR A 1 A_WeaponOffset(10, 47)
			PLZR C 1 A_WeaponOffset(32, 69)
			PLZR E 1 A_WeaponOffset(67, 100)
			TNT1 A 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS ABCCD 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSD D 1 A_PlaySound("GPIN")
			TOSS EF 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 8 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS G 1 A_PlaySound("GTOSS",CHAN_AUTO)
			TNT1 A 0 A_Takeinventory("HandGrenadeAmmo",1)
			TNT1 A 0 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS H 1 A_FireProjectile("ThrownGrenade",0,false,7,3,true,-6)
			TNT1 A 0 A_SetPitch(+2 + pitch)
			TOSS I 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS J 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 0 A_SetPitch(-1 + pitch)
			TOSS K 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TOSS L 1 A_WeaponReady(WRF_NoFire|WRF_NoSwitch)
			TNT1 A 9 A_WeaponOffset(67, 100)
			PLZR E 1 {
        A_WeaponOffset(67, 100);
        A_PlaySound("weapons/plasmagun/upsound", 0);
      }
			PLZR C 1 A_WeaponOffset(32, 69)
			PLZR A 1 A_WeaponOffset(10, 47)
			PLZG A 1 A_WeaponOffset(2, 34)
			TNT1 A 0 A_TakeInventory("GrenadeToss",1)
			Goto Ready
		NoAmmoWind:
			TNT1 A 0 A_GunFlash("UnZoomFlash")
		NoAmmo:
			"####" A 0 A_PlaySound("weapons/plasmagun/beep",0)
			"####" A 0 A_ReFire("NoAmmoFireLoop")
			NoAmmoReloadLoop:
				"####" A 1 A_WeaponReady(WRF_NOFIRE)
				"####" A 0 A_JumpIf(GetPlayerInput(INPUT_BUTTONS) & BT_RELOAD, "NoAmmoReloadLoop")
				Goto Ready
			NoAmmoFireLoop:
				"####" A 1 A_WeaponReady(WRF_NOFIRE)
				"####" A 0 A_ReFire("NoAmmoFireLoop")
				"####" A 0 A_ClearRefire
				Goto Ready
		Spawn:
			PLAS A -1
			Stop
	}
}

ACTOR Cellpak : Ammo
{
	+IGNORESKILL
	Inventory.MaxAmount 80
}

ACTOR Plasma : FastProjectile
{
	Scale 0.8
	Speed 80
	Damage 6
	DamageType Plasma
	SeeSound "none"
	DeathSound "weapons/plasmad/explode"
	Obituary "%o was liquified by %k's Pulse Rifle"
	Decal "PlasmaScorchLower"
	States
	{
	Spawn:
		PLZS AB 6 Bright
		Loop
	Death:
		TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
		TNT1 A 0 A_SpawnProjectile ("PlasmaSmoke", 1, 0, random (0, 360), 2, random (0, 160))
		PLZE ABCDE 4 Bright
		Stop
	}
}

ACTOR PulseEmpty : Inventory {
	Inventory.MaxAmount 1
}

Actor SpectreRemix : DemonRemix Replaces Spectre {
	+SHADOW
	RenderStyle Fuzzy
	SeeSound "spectre/sight"
	AttackSound "spectre/melee"
	PainSound "spectre/pain"
	DeathSound "spectre/death"
	ActiveSound "spectre/active"
	HitObituary "$OB_SPECTREHIT" // "%o was eaten by a spectre."
}

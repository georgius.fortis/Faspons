Actor CyberRemix : Cyberdemon Replaces Cyberdemon {
	Health 4000
	Radius 40
	Height 110
	Mass 1000
	Speed 16
	PainChance 5
	PainChance "SuperBuckshot", 7
	PainChance "Saw", 10
	Monster
	MinMissileChance 160
	+BOSS
	+MISSILEMORE
	+FLOORCLIP
	+NORADIUSDMG
	+DONTMORPH
	+BOSSDEATH
	-FRIGHTENED
	SeeSound "cyber/sight"
	PainSound "cyber/pain"
	DeathSound "cyber/death"
	ActiveSound "cyber/active"
	Obituary "$OB_CYBORG"
	HitObituary "%o was stomped into a fine paste by a Cyberdemon"
	States {
		Spawn:
			CYBR AB 10 A_Look
			Loop
		See:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==1, "SeeVanilla")
			TNT1 A 0 A_JumpIfInventory("LastStandEnabled", 1, 2)
			TNT1 A 0 A_JumpIfHealthLower(1501, "LastStand")
			CYBR A 3 A_Hoof
			CYBR ABBCC 3 A_Chase
			CYBR D 3 A_Metal
			CYBR D 3 A_Chase
			Loop
		SeeVanilla:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==0, "See")
			CYBR A 3 A_Hoof
			CYBR ABBCC 3 A_Chase(0, "MissileVanilla")
			CYBR D 3 A_Metal
			CYBR D 3 A_Chase(0, "MissileVanilla")
			Loop
		Melee:
			TNT1 A 0 A_FaceTarget
			TNT1 A 0 A_PlaySound("cyber/death", 7)
			CYBR Q 15
			TNT1 A 0 A_PlaySound("DSSTOMP", 7)
			TNT1 A 0 Radius_Quake(3, 26, 0, 14, 0)
			TNT1 A 0 A_Explode(50, 250, XF_NOTMISSILE, FALSE)
			TNT1 A 0 A_RadiusThrust(7000, 400, RTF_NOTMISSILE|RTF_NOIMPACTDAMAGE, 200)
			CYBR R 10
			Goto See
		Missile:
			TNT1 A 0 A_JumpIfCloser(125, "Melee")
			CYBR E 6 A_FaceTarget
			TNT1 A 0 A_PlaySound ("weapons/launcher/fire",7)
			CYBR F 12 A_SpawnProjectile("CyberRocket", 34, 0, 0, 0)
			CYBR E 12 A_FaceTarget
			TNT1 A 0 A_PlaySound ("weapons/launcher/fire",7)
			CYBR F 12 A_SpawnProjectile("CyberRocket", 34, 0, 0, 0)
			CYBR E 12 A_FaceTarget
			TNT1 A 0 A_PlaySound ("weapons/launcher/fire",7)
			CYBR F 12 A_SpawnProjectile("CyberRocket", 34, 0, 0, 0)
			Goto See
		MissileVanilla:
			CYBR E 6 A_FaceTarget
			CYBR F 12 A_CyberAttack
			CYBR E 12 A_FaceTarget
			CYBR F 12 A_CyberAttack
			CYBR E 12 A_FaceTarget
			CYBR F 12 A_CyberAttack
			Goto See
		LastStand:
			TNT1 A 0 A_RadiusGive("FearofMonster", 300, RGF_MONSTERS, 1)
			TNT1 A 0 A_ChangeFlag(NOPAIN, TRUE)
			TNT1 A 0 A_GiveInventory("LastStandEnabled", 1)
			TNT1 A 0 A_ChangeFlag(MISSILEMORE, TRUE)
			TNT1 A 0 A_ChangeFlag(ALWAYSFAST, TRUE)
			TNT1 A 0 A_PlaySound("cyber/death", 7)
			CYBR E 8 A_FaceTarget
			CYBR Q 15 A_FaceTarget
			TNT1 A 0 Radius_Quake(3, 26, 0, 50, 0)
			TNT1 A 0 A_Explode(50, 250, XF_NOTMISSILE, FALSE)
			TNT1 A 0 A_RadiusThrust(7000, 400, RTF_NOTMISSILE|RTF_NOIMPACTDAMAGE, 200)
			TNT1 A 0 A_PlaySound("DSSTOMP", 7)
			CYBR R 15 A_FaceTarget
			Goto See
		Pain:
			CYBR G 10 A_Pain
			Goto See
		Death:
			TNT1 AA 0 A_SpawnItem("RedBlood", 0, 30)
			TNT1 A 0 A_SpawnProjectile ("BigRedGibs", 32, 0, random (0, 360), 2, random (0, 160))
			TNT1 AAA 0 A_SpawnProjectile("RedGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			TNT1 AAA 0 A_SpawnProjectile("SmallRedGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			TNT1 A 0 Radius_Quake(3, 36, 0, 14, 0)
			CYBR H 4
			CYBR I 4 A_Scream
			CYBR JKL 4
			CYBR M 4 A_NoBlocking
			CYBR NO 4
			CYBR P 4
			CYBR P -1 A_BossDeath
			Stop
	}
}

Actor CyberRocket : Rocket {
	Radius 10
	Height 8
	Speed 20
	Damage 20
	Decal "BigScorch"
	Projectile
	Gravity 1.0
	ActiveSound "ROCKFLY"
	SeeSound "none"
	DeathSound "weapons/launcher/hit"
	Obituary "$OB_MPROCKET"
	States {
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, "NoSmoke")
			TNT1 A 0 A_ChangeFlag("ROCKETTRAIL", TRUE)
			Goto Shoot
		NoSmoke:
			TNT1 A 0 A_ChangeFlag("ROCKETTRAIL", FALSE)
		Shoot:
			MSLE A 1 Bright A_PlaySoundEx("ROCKFLY", "SoundSlot6", TRUE, -1)
			Loop
		Death:
			TNT1 A 0 {
				A_Explode;
				A_StopSoundEx("SoundSlot6");
			}
			TNT1 A 0 A_JumpIf(CallACS("GetDisableFancySmoke", 0, 0, 0)==1, 2)
			TNT1 A 0 A_SpawnProjectile ("RocketSmoke", 1, 0, random (0, 360), 2, random (0, 160))
			MACX ABCD 2 Bright
			MACX EFG 3 Bright
			MACX HIJ 3
			Stop
	}
}

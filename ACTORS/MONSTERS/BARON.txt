Actor BaronRemix : BaronOfHell Replaces BaronOfHell {
	Health 1000
	GibHealth 30
	BloodColor "Green"
	BloodType "GreenHitBlood"
	Decal "GreenFlyingBloodSplat"
	Radius 24
	Height 64
	Mass 1000
	Speed 8
	PainChance 5
	PainChance "SuperBuckshot", 25
	PainChance "Saw", 50
	Monster
	+FLOORCLIP
	+BOSSDEATH
	+MISSILEEVENMORE
	-FRIGHTENED
	SeeSound "baron/sight"
	PainSound "baron/pain"
	DeathSound "baron/death"
	ActiveSound "baron/active"
	Obituary "$OB_BARON"
	HitObituary "$OB_BARONHIT"
	States {
		Spawn:
			BOSS AB 10 A_Look
			Loop
		See:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==1, "SeeVanilla")
			TNT1 A 0 A_ChangeFlag("COUNTKILL", TRUE)
			BOSS AABB 3 A_Chase
			TNT1 A 0 A_JumpIfInventory("LastStandEnabled", 1, 2)
			TNT1 A 0 A_JumpIfHealthLower(301, "LastStand")
			BOSS CCDD 3 A_Chase
			Loop
		SeeVanilla:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==0, "See")
			BOSS AABBCCDD 3 A_Chase("MeleeVanilla", "MissileVanilla")
			Loop
		Melee:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==1, "MeleeVanilla")
			TNT1 A 0 A_Jump(256, "MeleeLeft", "MeleeRight")
			Goto MeleeLeft
		MeleeLeft:
			BOSS EF 8 A_FaceTarget
			BOSS G 8 A_CustomMeleeAttack(10 * RANDOM( 1, 8 ), "imp/melee", NONE, 0, 0)
			Goto See
		MeleeRight:
			BOSS PQ 8 A_FaceTarget
			BOSS R 8 A_CustomMeleeAttack(10 * RANDOM( 1, 8 ), "imp/melee", NONE, 0, 0)
			Goto See
		FastMelee:
			TNT1 A 0 A_Jump(256, "FastMeleeLeft", "FastMeleeRight")
			Goto FastMeleeLeft
		FastMeleeLeft:
			BOSS EF 4 A_FaceTarget
			BOSS G 4 A_CustomMeleeAttack(10 * RANDOM( 1, 8 ), "imp/melee", NONE, 0, 0)
			Goto See
		FastMeleeRight:
			BOSS PQ 4 A_FaceTarget
			BOSS R 4 A_CustomMeleeAttack(10 * RANDOM( 1, 8 ), "imp/melee", NONE, 0, 0)
			Goto See
		Missile:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==1, "MissileVanilla")
			TNT1 A 0 A_JumpIfInventory("LastStandEnabled", 1, "FastMissile")
			TNT1 A 0 A_Jump(32, "TriMissile")
			BOSS EF 8 A_FaceTarget
			TNT1 A 0 A_JumpIf(CallACS("GetFastMonstProjectiles", 0, 0, 0)==1, "FastBaronBall")
			BOSS G 8 A_SpawnProjectile("BaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			TNT1 A 0 A_JumpIfInventory("LastStandEnabled", 1, 2)
			TNT1 A 0 A_JumpIfHealthLower(301, "LastStand")
			BOSS PQ 8 A_MonsterRefire(130, "See")
			BOSS R 8 A_SpawnProjectile("BaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			Goto See
			FastBaronBall:
				BOSS G 8 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				TNT1 A 0 A_JumpIfInventory("LastStandEnabled", 1, 2)
				TNT1 A 0 A_JumpIfHealthLower(301, "LastStand")
				BOSS PQ 8 A_MonsterRefire(130, "See")
				BOSS R 8 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				Goto See
		FastMissile:
			TNT1 A 0 A_Jump(64, "FastTriMissile")
			BOSS EF 4 A_FaceTarget
			TNT1 A 0 A_JumpIf(CallACS("GetFastMonstProjectiles", 0, 0, 0)==1, "FasterFastBaronBall")
			BOSS G 4 A_SpawnProjectile("BaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			BOSS PQ 4 A_MonsterRefire(130, "See")
			BOSS R 4 A_SpawnProjectile("BaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			BOSS EF 4 A_MonsterRefire(130, "See")
			BOSS G 4 A_SpawnProjectile("BaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			Goto See
			FasterFastBaronBall:
				BOSS G 4 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				BOSS PQ 4 A_MonsterRefire(130, "See")
				BOSS R 4 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				BOSS EF 4 A_MonsterRefire(130, "See")
				BOSS G 4 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				Goto See
		TriMissile:
			BOSS P 20 A_FaceTarget
			BOSS Q 8 A_FaceTarget
			TNT1 A 0 A_JumpIf(CallACS("GetFastMonstProjectiles", 0, 0, 0)==1, "FastTriBaronBall")
			TNT1 A 0 A_SpawnProjectile("BaronBallRemix", 32, 0, -13, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			TNT1 A 0 A_SpawnProjectile("BaronBallRemix", 32, 0, 13, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			TNT1 A 0 A_SpawnProjectile("BaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			BOSS R 8
			Goto See
			FastTriBaronBall:
				TNT1 A 0 A_SpawnProjectile("FastBaronBallRemix", 32, 0, -13, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				TNT1 A 0 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 13, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				TNT1 A 0 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				BOSS R 8
				Goto See
		FastTriMissile:
			BOSS P 15 A_FaceTarget
			BOSS Q 4 A_FaceTarget
			TNT1 A 0 A_JumpIf(CallACS("GetFastMonstProjectiles", 0, 0, 0)==1, "FasterFastTriBaronBall")
			TNT1 A 0 A_SpawnProjectile("BaronBallRemix", 32, 0, -13, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			TNT1 A 0 A_SpawnProjectile("BaronBallRemix", 32, 0, 13, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			TNT1 A 0 A_SpawnProjectile("BaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
			BOSS R 4
			Goto See
			FasterFastTriBaronBall:
				TNT1 A 0 A_SpawnProjectile("FastBaronBallRemix", 32, 0, -13, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				TNT1 A 0 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 13, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				TNT1 A 0 A_SpawnProjectile("FastBaronBallRemix", 32, 0, 0, CMF_AIMOFFSET|CMF_AIMDIRECTION)
				BOSS R 4
				Goto See
		LastStand:
			TNT1 A 0 A_Jump(200, 2)
			TNT1 A 0 A_RadiusGive("FearofMonster", 250, RGF_MONSTERS, 1)
			TNT1 A 0 A_ChangeFlag(NOPAIN, TRUE)
			TNT1 A 0 A_GiveInventory("LastStandEnabled", 1)
			TNT1 A 0 A_ChangeFlag(MISSILEMORE, TRUE)
			TNT1 A 0 A_PlaySound("baron/sight", 7)
			TNT1 A 0 Radius_Quake(3, 10, 0, 50, 0)
			BOSS S 3 A_FaceTarget
			BOSS T 3 A_FaceTarget
			BOSS U 3 A_FaceTarget
			Goto See
		MeleeVanilla:
		MissileVanilla:
			BOSS EF 8 A_FaceTarget
			BOSS G 8 A_BruisAttack
			Goto See
		Pain:
			TNT1 A 0 A_JumpIf(CallACS("GetVanillaMonstersEnabled", 0, 0, 0)==1, 3)
			TNT1 A 0 A_JumpIfInventory("LastStandEnabled", 1, 2)
			TNT1 A 0 A_JumpIfHealthLower(301, "LastStand")
			BOSS H 2
			BOSS H 2 A_Pain
			Goto See
		Death:
			TNT1 A 0 A_TakeInventory("LastStandEnabled", 1)
			TNT1 A 0 A_GiveInventory("BaronIsDead", 1)
			TNT1 A 0 A_ChangeFlag(MISSILEMORE, FALSE)
			TNT1 A 0 A_ChangeFlag(NOPAIN, FALSE)
			TNT1 A 0 A_ChangeFlag(BRIGHT, FALSE)
			TNT1 AA 0 A_SpawnItem("GreenBlood", 0, 30)
			TNT1 AA 0 A_SpawnProjectile("GreenGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			TNT1 AA 0 A_SpawnProjectile("SmallGreenGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			BOSS I 4
			BOSS J 4 A_Scream
			BOSS K 4
			BOSS L 4 A_NoBlocking
			BOSS M 4
			BOSS N 4
			BOSS O -1 A_BossDeath
			Stop
		XDeath:
			TNT1 A 0 A_TakeInventory("LastStandEnabled", 1)
			TNT1 A 0 A_GiveInventory("BaronIsDead", 1)
			TNT1 A 0 A_GiveInventory("ExDeath", 1)
			TNT1 AA 0 A_SpawnItem("GreenBlood", 0, 30)
			TNT1 A 0 A_SpawnProjectile ("BigGreenGibs", 32, 0, random (0, 360), 2, random (0, 160))
			TNT1 AAA 0 A_SpawnProjectile("GreenGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			TNT1 AAA 0 A_SpawnProjectile("SmallGreenGibs", 10, 0, RANDOM(0, 360), 2, RANDOM(0, 160))
			BGIB A 4 A_Scream
			BGIB B 4 A_NoBlocking
			BGIB CDEFG 4
			BGIB H 4
			BGIB I -1 A_BossDeath
			Stop
		Death.SoulRes:
			TNT1 A 0 A_NoBlocking
			BOSS O -1
			Stop
		Raise:
			TNT1 A 0 A_CheckProximity("RaiseContinue", "ArchvileRemix", 100)
			TNT1 A 0 A_ChangeFlag("COUNTKILL", false)
			Goto SoulRaise
			RaiseContinue:
				TNT1 A 0 A_JumpIfInventory("ExDeath", 1, "ExRaise")
				BOSS O 1
				BOSS NMLKJI 8
				Goto See
		ExRaise:
			BGIB H 1
			BGIB GFEDCBA 4 A_TakeInventory("ExDeath", 1)
			Goto See
		SoulRaise:
			BOSS O 0 A_SpawnItem("BaronSoul", 0, 0)
			BOSS O 0 A_Die("SoulRes")
			Stop
	}
}

Actor BaronSoul : BaronRemix {
	+SHADOW
	Health 100
	Speed 5
	RenderStyle Translucent
	Alpha 0.5
	-SOLID
	+SHOOTABLE
	-COUNTKILL
	+NOBLOOD
	States {
		Spawn:
			TNT1 A 0
			TNT1 A 0 A_JumpIfInventory("ExDeath", 1, "ExSpawn", AAPTR_MASTER)
			BOSS O 4
			BOSS NMLKJI 4
			Goto Super::Spawn
		ExSpawn:
			BGIB HGFEDCBA 4
			Goto Super::Spawn
		Death:
			BOSS I 8 A_FadeOut(0.15)
			BOSS J 8 A_Scream
			BOSS K 8 A_FadeOut(0.15)
			BOSS L 8 A_NoBlocking
			BOSS MN 8 A_FadeOut(0.15)
			FadeD:
				BOSS O 1 A_FadeOut(0.15)
				Loop
		Raise:
			Stop
	}
}

Actor FastBaronBallRemix : BaronBallRemix {
	Speed 20
}

Actor LastStandEnabled : Inventory {
	Inventory.MaxAmount 1
}

Actor BaronIsDead : Inventory {
	Inventory.MaxAmount 1
}

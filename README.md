Check out the main thread here: http://forum.zdoom.org/viewtopic.php?f=19&t=48985
=================================================================================

This GIT houses the development of Faspons, a GZDOOM mod. You will see three
branches:

- Development: Development of newer changes for either testing or feedback
- Release: Stable release of Faspons
- Master: Base code in case I screw something up

If you have any questions, bug reports, feature requests, etc.; post in the
main thread or add an issue/feature request here in GitLab.

For any new changes, please check out the main ZDOOM thread.
